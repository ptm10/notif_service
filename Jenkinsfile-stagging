pipeline {
    agent any 
    tools {
        maven 'Maven 3.8.4'
    }
	
    environment {
        PROJECT_ID = 'ps-id-cerindocorp-14012022'
        CLUSTER_NAME = 'pmrms-stage'
		CLUSTER_NAME_PRE_PROD = 'pmrms-pre-prod'
        LOCATION = 'asia-southeast2-a'
        CREDENTIALS_ID = 'gke-private-key'
    }
    
    stages {
        stage('Checkout') {
            steps {
                checkout scm
            }
        }
        stage('Build image') {
            steps {
                script {
					sh "cp src/main/resources/application-stagging.properties src/main/resources/application.properties"
					sh "mvn clean package"
                    app = docker.build("mailpaps/pmrms-notif:${env.BUILD_ID}")
                }
            }
        }
        
        stage('Push image') {
            steps {
                script {
                    withCredentials( \
                                 [string(credentialsId: 'dockerhub',\
                                 variable: 'dockerhub')]) {
                        sh "docker login -u mailpaps -p ${dockerhub}"
                    }
                    app.push("${env.BUILD_ID}")
                 }
                                 
            }
        }
    
        stage('Deploy to K8s') {
            steps{
                echo "Deployment started ..."
                sh "sed -i 's/pmrms-notif:latest/pmrms-notif:${env.BUILD_ID}/g' deployment.yaml"
                step([$class: 'KubernetesEngineBuilder', \
                  projectId: env.PROJECT_ID, \
                  clusterName: env.CLUSTER_NAME, \
                  location: env.LOCATION, \
                  manifestPattern: 'deployment.yaml', \
                  credentialsId: env.CREDENTIALS_ID, \
                  verifyDeployments: true])
                }
            }
		stage('Build image Pre-Prod') {
            steps {
                script {
					sh "cp src/main/resources/application-pre-prod.properties src/main/resources/application.properties"
					sh "mvn clean package"
                    app = docker.build("mailpaps/pmrms-notif-pre-prod:${env.BUILD_ID}")
                }
            }
        }
        
        stage('Push image Pre-Prod') {
            steps {
                script {
                    withCredentials( \
                                 [string(credentialsId: 'dockerhub',\
                                 variable: 'dockerhub')]) {
                        sh "docker login -u mailpaps -p ${dockerhub}"
                    }
                    app.push("${env.BUILD_ID}")
                 }
                                 
            }
        }
    
        stage('Deploy to K8s Pre-Prod') {
            steps{
                echo "Deployment started ..."
                sh "sed -i 's/pmrms-notif-pre-prod:latest/pmrms-notif-pre-prod:${env.BUILD_ID}/g' deployment-pre-prod.yaml"
                step([$class: 'KubernetesEngineBuilder', \
                  projectId: env.PROJECT_ID, \
                  clusterName: env.CLUSTER_NAME_PRE_PROD, \
                  location: env.LOCATION, \
                  manifestPattern: 'deployment-pre-prod.yaml', \
                  credentialsId: env.CREDENTIALS_ID, \
                  verifyDeployments: true])
                }
            }
        }    
}
