package id.go.kemenag.madrasah.pmrms.notif

import id.go.kemenag.madrasah.pmrms.notif.model.request.EmailRequest
import id.go.kemenag.madrasah.pmrms.notif.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.notif.service.EmailService
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestMethodOrder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

@Suppress("UNCHECKED_CAST")
@TestMethodOrder(MethodOrderer.MethodName::class)
@SpringBootTest
class EmailServiceTest {

    @Autowired
    private lateinit var service: EmailService

    @Test
    @DisplayName("Test Send Email")
    fun test1() {
        println("Test Send Email")
        val test: ResponseEntity<ReturnData> =
            service.sendEamil(EmailRequest("ghithadinanhs@gmail.com", "test", "test"))
        Assertions.assertThat(test.statusCode).isNotEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
    }

}
