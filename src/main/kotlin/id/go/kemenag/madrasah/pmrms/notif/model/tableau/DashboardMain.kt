package id.go.kemenag.madrasah.pmrms.notif.model.tableau

data class DashboardMain(

    var datasetId: String? = null,

    var datasetOwner: String? = null,

    var tables: DashboardMainTable? = null,
)

data class DashboardMainTable(

    var tableName: String? = null,

    var columns: MutableList<DashboardMainTableCulumn>? = mutableListOf(),

    var data: MutableList<Map<String, String>>? = mutableListOf()
)

data class DashboardMainTableCulumn(

    var columnName: String? = null,

    var columnType: String? = null,

    var columnModel: String? = null
)

