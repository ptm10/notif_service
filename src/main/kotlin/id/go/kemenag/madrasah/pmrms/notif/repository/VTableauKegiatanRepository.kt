package id.go.kemenag.madrasah.pmrms.notif.repository

import id.go.kemenag.madrasah.pmrms.notif.pojo.VTableauKegiatan
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface VTableauKegiatanRepository : JpaRepository<VTableauKegiatan, String> {

    @Query("select v from VTableauKegiatan v")
    fun getAllKegiatan(): List<VTableauKegiatan>
}
