package id.go.kemenag.madrasah.pmrms.notif.controller

import id.go.kemenag.madrasah.pmrms.notif.model.request.EmailRequest
import id.go.kemenag.madrasah.pmrms.notif.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.notif.service.EmailService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid

@Api(tags = ["Email"], description = "Email API")
@RestController
@RequestMapping(path = ["email"])
class EmailController {

    @Autowired
    private lateinit var service: EmailService

    @PostMapping(value = ["send"], produces = ["application/json"])
    fun sendEmail(
        @Valid @RequestBody request: EmailRequest,
        httpServletRequest: HttpServletRequest? = null
    ): ResponseEntity<ReturnData> {
        return service.sendEamil(request)
    }
}
