package id.go.kemenag.madrasah.pmrms.notif.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "v_tableau_awp", schema = "public")
data class VTableauAwp(
    @Id
    @Column(name = "component_code_code")
    var componentCodeCode: String? = null,

    @Column(name = "component_code")
    var componentCode: String? = null,

    @Column(name = "kegiatan")
    var kegiatan: String? = null,

    @Column(name = "year")
    var year: Int? = null,

    @Column(name = "province")
    var province: String? = null,

    @Column(name = "regency")
    var regency: String? = null,

    @Column(name = "start_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "GMT+7")
    var startDate: Date? = null,

    @Column(name = "end_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "GMT+7")
    var endDate: Date? = null,
)
