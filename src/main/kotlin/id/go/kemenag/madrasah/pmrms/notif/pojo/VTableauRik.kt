package id.go.kemenag.madrasah.pmrms.notif.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "v_tableau_rik", schema = "public")
data class VTableauRik(
    @Id
    @Column(name = "komponen")
    var komponen: String? = null,

    @Column(name = "kode")
    var kode: String? = null,

    @Column(name = "kegiatan")
    var kegiatan: String? = null,

    @Column(name = "start_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "GMT+7")
    var start_date: Date? = null,

    @Column(name = "end_date")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "GMT+7")
    var end_date: Date? = null,

    @Column(name = "regexp_replace")
    var regexp_replace: String? = null,

    @Column(name = "name")
    var name: String? = null,

    @Column(name = "prov")
    var prov: String? = null,

    @Column(name = "kokab")
    var kokab: String? = null,

    @Column(name = "ns_count")
    var ns_count: String? = null,

    @Column(name = "participant_count")
    var participant_count: String? = null,

    @Column(name = "event_volume")
    var event_volume: String? = null,

    @Column(name = "risiko")
    var risiko: String? = null,

    @Column(name = "impact")
    var impact: Int? = null,

    @Column(name = "potential")
    var potential: Int? = null,

)
