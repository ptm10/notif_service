package id.go.kemenag.madrasah.pmrms.notif.helpers

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import id.go.kemenag.madrasah.pmrms.notif.constant.HEADER_STRING
import id.go.kemenag.madrasah.pmrms.notif.model.response.ReturnData
import kong.unirest.Unirest
import org.apache.http.client.HttpClient
import org.apache.http.conn.ssl.NoopHostnameVerifier
import org.apache.http.conn.ssl.TrustSelfSignedStrategy
import org.apache.http.impl.client.HttpClients
import org.apache.http.ssl.SSLContextBuilder
import java.security.cert.X509Certificate
import javax.net.ssl.SSLContext

class RequestHelpers {

    companion object {
        private fun configHttpClient() {
            val sslContext: SSLContext = SSLContextBuilder().loadTrustMaterial(
                null,
                object : TrustSelfSignedStrategy() {
                    override fun isTrusted(chain: Array<X509Certificate?>?, authType: String?): Boolean {
                        return true
                    }
                }
            ).build()

            val customHttpClient: HttpClient = HttpClients.custom().setSSLContext(sslContext)
                .setSSLHostnameVerifier(NoopHostnameVerifier()).build()

            Unirest.config().httpClient(customHttpClient)
        }

        fun authDetail(authUrl: String, bearer: String): ReturnData? {
            return try {
                configHttpClient()
                val objectMapper = ObjectMapper()
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

                val reqUrl = "$authUrl/detail"
                val response = Unirest.get(reqUrl)
                    .header("Content-Type", "application/json")
                    .header(HEADER_STRING, bearer)
                    .asString()

                objectMapper.readValue(response.body, ReturnData::class.java)
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        }

    }
}
