package id.go.kemenag.madrasah.pmrms.notif.controller

import id.go.kemenag.madrasah.pmrms.notif.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.notif.service.TableauKegiatanLaporananBulananService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Api(tags = ["Tableau Kegiatan Laporan Bulanan"], description = "Tableau Kegiatan Laporan Bulanan API")
@RestController
@RequestMapping(path = ["tableau-kegiatan-laporan-bulanan"])
class TableauKegiatanLaporanBulananController {

    @Autowired
    private lateinit var service: TableauKegiatanLaporananBulananService

    @GetMapping(value = ["get-all-kegiatan-laporan-bulanan"], produces = ["application/json"])
    fun getAllKegiatanLaporanBulanan(): ResponseEntity<ReturnData> {
        return service.getAllKegiatanLaporanBulanan()
    }
}
