package id.go.kemenag.madrasah.pmrms.notif.service

import id.go.kemenag.madrasah.pmrms.notif.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.notif.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.notif.repository.VTableauInformasiBudgetRealisasiRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Service
class TableauInformasiBudgetRealisasiService {

    @Autowired
    private lateinit var repoVTableauInformasiBudgetRealisasi: VTableauInformasiBudgetRealisasiRepository

    fun getAllInformasiBudgetResiko(): ResponseEntity<ReturnData> {
        try {
            return responseSuccess(data = repoVTableauInformasiBudgetRealisasi.getAllInformasiBudgetRealisasi())
        } catch (e: Exception) {
            throw e
        }
    }
}
