package id.go.kemenag.madrasah.pmrms.notif.controller

import id.go.kemenag.madrasah.pmrms.notif.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.notif.service.TableauEventService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Api(tags = ["Tableau Event"], description = "Tableau Event API")
@RestController
@RequestMapping(path = ["tableau-event"])
class TableauEventController {

    @Autowired
    private lateinit var service: TableauEventService

    @GetMapping(value = ["get-all-event"], produces = ["application/json"])
    fun getAllEvent(): ResponseEntity<ReturnData> {
        return service.getAllEvent()
    }
}
