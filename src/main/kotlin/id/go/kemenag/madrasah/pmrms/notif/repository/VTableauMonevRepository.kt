package id.go.kemenag.madrasah.pmrms.notif.repository

import id.go.kemenag.madrasah.pmrms.notif.pojo.VTableauMonev
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface VTableauMonevRepository : JpaRepository<VTableauMonev, String> {

    @Query("select v from VTableauMonev v")
    fun getAllMonev(): List<VTableauMonev>
}
