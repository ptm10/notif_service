package id.go.kemenag.madrasah.pmrms.notif.constant

const val EXPIRATION_TIME: Long = 864000000 // 10 days (1 hour = 3600000)
const val SECRET = "PMRMS-2022"
const val TOKEN_PREFIX = "Bearer "
const val HEADER_STRING = "Authorization"

val USER_ADMIN_ALLOWED_PATH =
    listOf(
        "/users/*",
        "/auth/logout",
        "/auth/detail",
        "/profile/*",
    )

val AUDIENCE_FILTER_PATH = mapOf(
    "user-admin" to USER_ADMIN_ALLOWED_PATH
)
