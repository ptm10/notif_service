package id.go.kemenag.madrasah.pmrms.notif.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "v_tableau_event", schema = "public")
data class VTableauEvent(
    @Id
    @Column(name = "id")
    var id: Int? = null,

    @Column(name = "komponen")
    var komponen: String? = null,

    @Column(name = "kode_kegiatan")
    var kode_kegiatan: String? = null,

    @Column(name = "kegiatan")
    var kegiatan: String? = null,

    @Column(name = "event")
    var event: String? = null,

    @Column(name = "tahun")
    var tahun: Int? = null,

    @Column(name = "bulan")
    var bulan: Int? = null,

    @Column(name = "jenis_pelaksanaan_rencana")
    var jenis_pelaksanaan_rencana: String? = null,

    @Column(name = "jenis_kegiatan_rencana")
    var jenis_kegiatan_rencana: String? = null,

    @Column(name = "tgl_mulai_rencana")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var tgl_mulai_rencana: Date? = null,

    @Column(name = "tgl_selesai_rencana")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var tgl_selesai_rencana: Date? = null,

    @Column(name = "tgl_mulai_laporan")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var tgl_mulai_laporan: Date? = null,

    @Column(name = "tgl_selesai_laporan")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "GMT+7")
    var tgl_selesai_laporan: Date? = null,

    @Column(name = "provinsi_rencana")
    var provinsi_rencana: String? = null,

    @Column(name = "provinsi_laporan")
    var provinsi_laporan: String? = null,

    @Column(name = "kokab_rencana")
    var kokab_rencana: String? = null,

    @Column(name = "kokab_laporan")
    var kokab_laporan: String? = null,

    @Column(name = "anggaran_rencana")
    var anggaran_rencana: Long? = null,

    @Column(name = "anggaran_laporan")
    var anggaran_laporan: Long? = null,

    @Column(name = "jml_peserta_rencana")
    var jml_peserta_rencana: Long? = null,

    @Column(name = "jml_peserta_laporan")
    var jml_peserta_laporan: Long? = null,

    @Column(name = "jml_peserta_wanita_laporan")
    var jml_peserta_wanita_laporan: Long? = null,

    @Column(name = "jml_peserta_pria_laporan")
    var jml_peserta_pria_laporan: Long? = null,

    @Column(name = "penanggung_jawab")
    var penanggung_jawab: String? = null,

    @Column(name = "status_rencana")
    var status_rencana: Int? = null,

    @Column(name = "compliance_monev")
    var compliance_monev: Long? = null,

    @Column(name = "ekonomis_monev")
    var ekonomis_monev: Long? = null,

    @Column(name = "efektif_monev")
    var efektif_monev: Long? = null,

    @Column(name = "efisien_monev")
    var efisien_monev: Long? = null,

    @Column(name = "compliance_peserta")
    var compliance_peserta: Long? = null,

    @Column(name = "ekonomis_peserta")
    var ekonomis_peserta: Long? = null,

    @Column(name = "efektif_peserta")
    var efektif_peserta: Long? = null,

    @Column(name = "efisien_peserta")
    var efisien_peserta: Long? = null,

    @Column(name = "status_laporan")
    var status_laporan: Int? = null
)
