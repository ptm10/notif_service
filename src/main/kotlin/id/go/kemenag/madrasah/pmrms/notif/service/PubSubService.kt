package id.go.kemenag.madrasah.pmrms.notif.service

import com.fasterxml.jackson.databind.ObjectMapper
import com.google.api.gax.rpc.ApiException
import com.google.cloud.spring.pubsub.core.publisher.PubSubPublisherTemplate
import id.go.kemenag.madrasah.pmrms.notif.helpers.dateToString
import id.go.kemenag.madrasah.pmrms.notif.helpers.publishWithErrorHandler
import id.go.kemenag.madrasah.pmrms.notif.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.notif.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.notif.model.tableau.DashboardMain
import id.go.kemenag.madrasah.pmrms.notif.model.tableau.DashboardMainTable
import id.go.kemenag.madrasah.pmrms.notif.model.tableau.DashboardMainTableCulumn
import id.go.kemenag.madrasah.pmrms.notif.repository.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.time.Year

@Service
class PubSubService {

    @Autowired
    private lateinit var pubSubTemplate: PubSubPublisherTemplate

    @Autowired
    private lateinit var repoVTableauAwp: VTableauAwpRepository

    @Autowired
    private lateinit var repoVTableauAwp2: VTableauAwp2Repository

    @Autowired
    private lateinit var repoVTableauMonev: VTableauMonevRepository

    @Autowired
    private lateinit var repoVTableRik: VTableauRikRepository

    @Autowired
    private lateinit var repoVTableKegiatan: VTableauKegiatanRepository

    @Autowired
    private lateinit var repoVTableKegiatanLaporanBulanan: VTableauKegiatanLaporanBulananRepository

    @Autowired
    private lateinit var repoVTableEvent: VTableauEventRepository

    @Autowired
    private lateinit var repoVTableInformasiResiko: VTableauInformasiResikoRepository

    @Autowired
    private lateinit var repoVTableInformasiBudgetRealisasi: VTableauInformasiBudgetRealisasiRepository

    fun testSendMessage(): ResponseEntity<ReturnData> {
        try {
            val message = pubSubTemplate.publish("dashboard-main", "test")

            message.addCallback({
                println(it)
            }, {
                it.printStackTrace()
            })

            return responseSuccess(data = message)
        } catch (e: ApiException) {
            throw e
        }
    }

    fun sendAllMessage() {
        try {
            val messages = mutableListOf<String>()
            messages.add(awp2Message())
            messages.add(monevMessage())
            messages.add(kegiatanMessage())
            messages.add(eventMessage())
            /*
            messages.add(kegiatanLaporanBulananMessage())
            messages.add(rikMessage())
            messages.add(informasiResikoMessage())
            messages.add(InformasiBudgetRealisasiMessage())
            */
            publishWithErrorHandler(pubSubTemplate, "dashboard-truncate", messages)
        } catch (e: Exception) {
            throw e
        }
    }

    fun sendMessageAwp(): ResponseEntity<ReturnData> {
        try {
            val message = awpMessage()
            pubSubTemplate.publish("dashboard-truncate", message).addCallback({
                println("sendMessageAWPSuccess : $it")
            }, {
                it.printStackTrace()
            })

            return responseSuccess(data = message)
        } catch (e: Exception) {
            throw e
        }
    }

    fun sendMessageAwp2(): ResponseEntity<ReturnData> {
        try {
            val message = awp2Message()
            pubSubTemplate.publish("dashboard-truncate", message).addCallback({
                println("sendMessageAwp2Success : $it")
            }, {
                it.printStackTrace()
            })

            return responseSuccess(data = message)
        } catch (e: Exception) {
            throw e
        }
    }

    fun sendMessageRik(): ResponseEntity<ReturnData> {
        try {
            val message = rikMessage()
            pubSubTemplate.publish("dashboard-truncate", message).addCallback({
                println("sendMessageRikSuccess : $it")
            }, {
                it.printStackTrace()
            })

            return responseSuccess(data = message)
        } catch (e: Exception) {
            throw e
        }
    }

    fun sendMessageMonev(): ResponseEntity<ReturnData> {
        try {
            val message = monevMessage()
            pubSubTemplate.publish("dashboard-truncate", message).addCallback({
                println("sendMessageMonevSuccess : $it")
            }, {
                it.printStackTrace()
            })

            return responseSuccess(data = message)
        } catch (e: Exception) {
            throw e
        }
    }

    fun sendMessageKegiatanLaporanBulanan(): ResponseEntity<ReturnData> {
        try {
            val message = kegiatanLaporanBulananMessage()
            pubSubTemplate.publish("dashboard-truncate", message).addCallback({
                println("sendMessageKegiatanSuccess : $it")
            }, {
                it.printStackTrace()
            })

            return responseSuccess(data = message)
        } catch (e: Exception) {
            throw e
        }
    }

    fun sendMessageKegiatan(): ResponseEntity<ReturnData> {
        try {
            val message = kegiatanMessage()
            pubSubTemplate.publish("dashboard-truncate", message).addCallback({
                println("sendMessageKegiatanSuccess : $it")
            }, {
                it.printStackTrace()
            })

            return responseSuccess(data = message)
        } catch (e: Exception) {
            throw e
        }
    }

    fun sendMessageEvent(): ResponseEntity<ReturnData> {
        try {
            val message = eventMessage()
            pubSubTemplate.publish("dashboard-truncate", message).addCallback({
                println("sendMessageEventSuccess : $it")
            }, {
                it.printStackTrace()
            })

            return responseSuccess(data = message)
        } catch (e: Exception) {
            throw e
        }
    }

    fun sendMessageInformasiResiko(): ResponseEntity<ReturnData> {
        try {
            val message = informasiResikoMessage()
            pubSubTemplate.publish("dashboard-truncate", ObjectMapper().writeValueAsString(message)).addCallback({
                println("sendMessageInformasiResikoSuccess : $it")
            }, {
                it.printStackTrace()
            })

            return responseSuccess(data = message)
        } catch (e: Exception) {
            throw e
        }
    }

    fun sendMessageInformasiBudgetRealisasi(): ResponseEntity<ReturnData> {
        try {
            val message = InformasiBudgetRealisasiMessage()
            pubSubTemplate.publish("dashboard-truncate", message).addCallback({
                println("sendMessageInformasiBudgedRealisasiSuccess : $it")
            }, {
                it.printStackTrace()
            })

            return responseSuccess(data = message)
        } catch (e: Exception) {
            throw e
        }
    }

    fun awpMessage(): String {
        try {
            val dashboardMain = DashboardMain()
            dashboardMain.datasetId = "pmrms"
            dashboardMain.datasetOwner = "pmrms"

            val dashboardMainTable = DashboardMainTable()
            dashboardMainTable.tableName = "awp_prod"

            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "component_code_code", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "component_code", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "kegiatan", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "year", "INT64", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "province", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "regency", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "start_date", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "end_date", "STRING", "NULLABLE"
                )
            )

            repoVTableauAwp.getByYear(Year.now().value).forEach { d ->
                val data = mutableMapOf<String, String>()

                d.componentCodeCode?.let {
                    data["component_code_code"] = it
                }
                d.componentCode?.let {
                    data["component_code"] = it
                }
                d.kegiatan?.let {
                    data["kegiatan"] = it
                }
                d.year?.let {
                    data["year"] = it.toString()
                }
                d.province?.let {
                    data["province"] = it
                }
                d.regency?.let {
                    data["regency"] = it
                }
                d.startDate?.let {
                    data["start_date"] = dateToString(it)
                }
                d.endDate?.let {
                    data["end_date"] = dateToString(it)
                }

                dashboardMainTable.data?.add(data)
            }

            dashboardMain.tables = dashboardMainTable


            return ObjectMapper().writeValueAsString(dashboardMain)
        } catch (e: Exception) {
            throw e
        }
    }

    fun awp2Message(): String {
        try {
            val dashboardMain = DashboardMain()
            dashboardMain.datasetId = "pmrms"
            dashboardMain.datasetOwner = "pmrms"

            val dashboardMainTable = DashboardMainTable()
            dashboardMainTable.tableName = "awp_prod"


            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "komponen", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "kode_kegiatan", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "kegiatan", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "tahun", "INT64", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "jenis_pelaksanaan", "STRING", "NULLABLE"
                )
            )

            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "jenis_kegiatan", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "tgl_mulai", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "tgl_selesai", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "provinsi", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "kokab", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "anggaran_awp", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "jml_event", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "jml_narsum", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "jml_peserta", "NUMERIC", "NULLABLE"
                )
            )

            repoVTableauAwp2.getAllAwp().forEach { d ->
                val data = mutableMapOf<String, String>()

                d.komponen?.let {
                    data["komponen"] = it
                }
                d.kode_kegiatan?.let {
                    data["kode_kegiatan"] = it
                }
                d.kegiatan?.let {
                    data["kegiatan"] = it
                }
                d.tahun?.let {
                    data["tahun"] = it.toString()
                }
                d.jenis_pelaksanaan?.let {
                    data["jenis_pelaksanaan"] = it
                }
                d.jenis_kegiatan?.let {
                    data["jenis_kegiatan"] = it
                }
                d.tgl_mulai?.let {
                    data["tgl_mulai"] = dateToString(date = it, format = "yyyy-MM-dd")
                }
                d.tgl_selesai?.let {
                    data["tgl_selesai"] = dateToString(date = it, format = "yyyy-MM-dd")
                }
                d.provinsi?.let {
                    data["provinsi"] = it
                }
                d.kokab?.let {
                    data["kokab"] = it
                }
                d.anggaran_awp?.let {
                    data["anggaran_awp"] = it.toString()
                }
                d.jml_event?.let {
                    data["jml_event"] = it.toString()
                }
                d.jml_narsum?.let {
                    data["jml_narsum"] = it.toString()
                }
                d.jml_peserta?.let {
                    data["jml_peserta"] = it.toString()
                }

                dashboardMainTable.data?.add(data)
            }

            dashboardMain.tables = dashboardMainTable


            return ObjectMapper().writeValueAsString(dashboardMain)
        } catch (e: Exception) {
            throw e
        }
    }

    fun monevMessage(): String {
        try {
            val dashboardMain = DashboardMain()
            dashboardMain.datasetId = "pmrms"
            dashboardMain.datasetOwner = "pmrms"

            val dashboardMainTable = DashboardMainTable()
            dashboardMainTable.tableName = "monev_prod"

            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "tahun", "INT64", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "rc_komponen", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "rc_sub_komponen", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "rc_code", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "rc_nama", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "rc_parent", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "rc_status2020", "INT64", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "rc_status2021", "INT64", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "rc_status2022", "INT64", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "rc_status2023", "INT64", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "rc_status2024", "INT64", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "si_code", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "si_name", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "pdo_code", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "pdo_name", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "iri_code", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "iri_name", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "si_baseline", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "si_target_total", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "si_target2020", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "si_target2021", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "si_target2022", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "si_target2023", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "si_target2024", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "kegiatan_kode", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "kegiatan_name", "STRING", "NULLABLE"
                )
            )

            repoVTableauMonev.getAllMonev().forEach { d ->
                val data = mutableMapOf<String, String>()

                d.tahun?.let {
                    data["tahun"] = it.toString()
                }
                d.rc_komponen?.let {
                    data["rc_komponen"] = it
                }
                d.rc_sub_komponen?.let {
                    data["rc_sub_komponen"] = it
                }
                d.rc_code?.let {
                    data["rc_code"] = it
                }
                d.rc_nama?.let {
                    data["rc_nama"] = it
                }
                d.rc_parent?.let {
                    data["rc_parent"] = it
                }
                d.rc_status2020?.let {
                    data["rc_status2020"] = it.toString()
                }
                d.rc_status2021?.let {
                    data["rc_status2021"] = it.toString()
                }
                d.rc_status2022?.let {
                    data["rc_status2022"] = it.toString()
                }
                d.rc_status2023?.let {
                    data["rc_status2023"] = it.toString()
                }
                d.rc_status2024?.let {
                    data["rc_status2024"] = it.toString()
                }
                d.si_code?.let {
                    data["si_code"] = it
                }
                d.si_name?.let {
                    data["si_name"] = it
                }
                d.pdo_code?.let {
                    data["pdo_code"] = it
                }
                d.pdo_name?.let {
                    data["pdo_name"] = it
                }
                d.iri_code?.let {
                    data["iri_code"] = it
                }
                d.iri_name?.let {
                    data["iri_name"] = it
                }
                d.si_baseline?.let {
                    data["si_baseline"] = it
                }
                d.si_target_total?.let {
                    data["si_target_total"] = it
                }
                d.si_target2020?.let {
                    data["si_target2020"] = it
                }
                d.si_target2021?.let {
                    data["si_target2021"] = it
                }
                d.si_target2022?.let {
                    data["si_target2022"] = it
                }
                d.si_target2023?.let {
                    data["si_target2023"] = it
                }
                d.si_target2024?.let {
                    data["si_target2024"] = it
                }
                d.kegiatan_kode?.let {
                    data["kegiatan_kode"] = it
                }
                d.kegiatan_name?.let {
                    data["kegiatan_name"] = it
                }

                dashboardMainTable.data?.add(data)
            }

            dashboardMain.tables = dashboardMainTable


            return ObjectMapper().writeValueAsString(dashboardMain)
        } catch (e: Exception) {
            throw e
        }
    }

    fun rikMessage(): String {
        try {
            val dashboardMain = DashboardMain()
            dashboardMain.datasetId = "pmrms"
            dashboardMain.datasetOwner = "pmrms"

            val dashboardMainTable = DashboardMainTable()
            dashboardMainTable.tableName = "rik_prod"

            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "komponen", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "kode", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "kegiatan", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "start_date", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "end_date", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "regexp_replace", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "name", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "prov", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "kokab", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "ns_count", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "participant_count", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "event_volume", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "risiko", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "impact", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "potential", "STRING", "NULLABLE"
                )
            )

            repoVTableRik.getAllRik().forEach {
                val data = mutableMapOf<String, String>()
                data["komponen"] = it.komponen ?: ""
                data["kode"] = it.kode ?: ""
                data["kegiatan"] = it.kegiatan ?: ""
                data["start_date"] = dateToString(it.start_date)
                data["end_date"] = dateToString(it.end_date)
                data["regexp_replace"] = it.regexp_replace ?: ""
                data["name"] = it.name ?: ""
                data["prov"] = it.prov ?: ""
                data["kokab"] = it.kokab ?: ""
                data["ns_count"] = it.ns_count ?: ""
                data["participant_count"] = it.participant_count ?: ""
                data["event_volume"] = it.event_volume ?: ""
                data["risiko"] = it.risiko ?: ""
                data["impact"] = it.impact?.toString() ?: ""
                data["potential"] = it.potential?.toString() ?: ""

                dashboardMainTable.data?.add(data)

            }

            dashboardMain.tables = dashboardMainTable

            return ObjectMapper().writeValueAsString(dashboardMain)
        } catch (e: Exception) {
            throw e
        }
    }

    fun kegiatanLaporanBulananMessage(): String {
        try {
            val dashboardMain = DashboardMain()
            dashboardMain.datasetId = "pmrms"
            dashboardMain.datasetOwner = "pmrms"

            val dashboardMainTable = DashboardMainTable()
            dashboardMainTable.tableName = "kegiatan_laporan_bulanan_prod"

            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "komponen", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "kode_kegiatan", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "kegiatan", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "tahun", "INT64", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "bulan", "INT64", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "jenis_pelaksanaan_rencana", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "jenis_kegiatan_rencana", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "tgl_mulai_rencana", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "tgl_selesai_rencana", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "tgl_mulai_laporan", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "tgl_selesai_laporan", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "provinsi_rencana", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "provinsi_laporan", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "kokab_rencana", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "kokab_laporan", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "anggaran_awp", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "anggaran_pok", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "anggaran_laporan", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "anggaran_laporan01", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "anggaran_laporan02", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "anggaran_laporan03", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "anggaran_laporan04", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "anggaran_laporan05", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "anggaran_laporan06", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "anggaran_laporan07", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "anggaran_laporan08", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "anggaran_laporan09", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "anggaran_laporan10", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "anggaran_laporan11", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "anggaran_laporan12", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "jml_event_rencana", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "jml_narsum_rencana", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "jml_peserta_rencana", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "jml_peserta_laporan", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "jml_peserta_pria_laporan", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "jml_peserta_wanita_laporan", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "penanggung_jawab", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "pic_lsp", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "risiko", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "impak", "INT64", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "potensi", "INT64", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "nilai_qae", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "compliance_monev", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "ekonomis_monev", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "efektif_monev", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "efisien_monev", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "compliance_peserta", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "ekonomis_peserta", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "efektif_peserta", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "efisien_peserta", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "status_rencana", "INT64", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "status_laporan", "INT64", "NULLABLE"
                )
            )

            repoVTableKegiatanLaporanBulanan.getAllKegiatanLaporanBulanan().forEach { d ->
                val data = mutableMapOf<String, String>()

                d.komponen?.let {
                    data["komponen"] = it
                }
                d.kode_kegiatan?.let {
                    data["kode_kegiatan"] = it
                }
                d.kegiatan?.let {
                    data["kegiatan"] = it
                }
                d.tahun?.let {
                    data["tahun"] = it.toString()
                }
                d.bulan?.let {
                    data["bulan"] = it.toString()
                }
                d.jenis_pelaksanaan_rencana?.let {
                    data["jenis_pelaksanaan_rencana"] = it
                }
                d.jenis_kegiatan_rencana?.let {
                    data["jenis_kegiatan_rencana"] = it
                }
                d.tgl_mulai_rencana?.let {
                    data["tgl_mulai_rencana"] = dateToString(date = it, format = "yyyy-MM-dd")
                }
                d.tgl_selesai_rencana?.let {
                    data["tgl_selesai_rencana"] = dateToString(date = it, format = "yyyy-MM-dd")
                }
                d.tgl_mulai_laporan?.let {
                    data["tgl_mulai_laporan"] = dateToString(date = it, format = "yyyy-MM-dd")
                }
                d.tgl_selesai_laporan?.let {
                    data["tgl_selesai_laporan"] = dateToString(date = it, format = "yyyy-MM-dd")
                }
                d.provinsi_rencana?.let {
                    data["provinsi_rencana"] = it
                }
                d.provinsi_laporan?.let {
                    data["provinsi_laporan"] = it
                }
                d.kokab_rencana?.let {
                    data["kokab_rencana"] = it
                }
                d.kokab_laporan?.let {
                    data["kokab_laporan"] = it
                }
                d.anggaran_awp?.let {
                    data["anggaran_awp"] = it.toString()
                }
                d.anggaran_pok?.let {
                    data["anggaran_pok"] = it.toString()
                }
                d.anggaran_laporan?.let {
                    data["anggaran_laporan"] = it.toString()
                }
                d.anggaran_laporan01?.let {
                    data["anggaran_laporan01"] = it.toString()
                }
                d.anggaran_laporan02?.let {
                    data["anggaran_laporan02"] = it.toString()
                }
                d.anggaran_laporan03?.let {
                    data["anggaran_laporan03"] = it.toString()
                }
                d.anggaran_laporan04?.let {
                    data["anggaran_laporan04"] = it.toString()
                }
                d.anggaran_laporan05?.let {
                    data["anggaran_laporan05"] = it.toString()
                }
                d.anggaran_laporan06?.let {
                    data["anggaran_laporan06"] = it.toString()
                }
                d.anggaran_laporan07?.let {
                    data["anggaran_laporan07"] = it.toString()
                }
                d.anggaran_laporan08?.let {
                    data["anggaran_laporan08"] = it.toString()
                }
                d.anggaran_laporan09?.let {
                    data["anggaran_laporan09"] = it.toString()
                }
                d.anggaran_laporan10?.let {
                    data["anggaran_laporan10"] = it.toString()
                }
                d.anggaran_laporan11?.let {
                    data["anggaran_laporan11"] = it.toString()
                }
                d.anggaran_laporan12?.let {
                    data["anggaran_laporan12"] = it.toString()
                }
                d.jml_event_rencana?.let {
                    data["jml_event_rencana"] = it.toString()
                }
                d.jml_narsum_rencana?.let {
                    data["jml_narsum_rencana"] = it.toString()
                }
                d.jml_peserta_rencana?.let {
                    data["jml_peserta_rencana"] = it.toString()
                }
                d.jml_peserta_laporan?.let {
                    data["jml_peserta_laporan"] = it.toString()
                }
                d.jml_peserta_pria_laporan?.let {
                    data["jml_peserta_pria_laporan"] = it.toString()
                }
                d.jml_peserta_wanita_laporan?.let {
                    data["jml_peserta_wanita_laporan"] = it.toString()
                }
                d.penanggung_jawab?.let {
                    data["penanggung_jawab"] = it
                }
                d.pic_lsp?.let {
                    data["pic_lsp"] = it
                }
                d.risiko?.let {
                    data["risiko"] = it
                }
                d.impak?.let {
                    data["impak"] = it.toString()
                }
                d.potensi?.let {
                    data["potensi"] = it.toString()
                }
                d.nilai_qae?.let {
                    data["nilai_qae"] = it.toString()
                }
                d.compliance_monev?.let {
                    data["compliance_monev"] = it.toString()
                }
                d.ekonomis_monev?.let {
                    data["ekonomis_monev"] = it.toString()
                }
                d.efektif_monev?.let {
                    data["efektif_monev"] = it.toString()
                }
                d.efisien_monev?.let {
                    data["efisien_monev"] = it.toString()
                }
                d.compliance_peserta?.let {
                    data["compliance_peserta"] = it.toString()
                }
                d.ekonomis_peserta?.let {
                    data["ekonomis_peserta"] = it.toString()
                }
                d.efektif_peserta?.let {
                    data["efektif_peserta"] = it.toString()
                }
                d.efisien_peserta?.let {
                    data["efisien_peserta"] = it.toString()
                }
                d.status_rencana?.let {
                    data["status_rencana"] = it.toString()
                }
                d.status_laporan?.let {
                    data["status_laporan"] = it.toString()
                }

                dashboardMainTable.data?.add(data)
            }

            dashboardMain.tables = dashboardMainTable


            return ObjectMapper().writeValueAsString(dashboardMain)
        } catch (e: Exception) {
            throw e
        }
    }

    fun kegiatanMessage(): String {
        try {
            val dashboardMain = DashboardMain()
            dashboardMain.datasetId = "pmrms"
            dashboardMain.datasetOwner = "pmrms"

            val dashboardMainTable = DashboardMainTable()
            dashboardMainTable.tableName = "kegiatan_prod"

            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "komponen", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "kode_kegiatan", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "kegiatan", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "tahun", "INT64", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "bulan", "INT64", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "jenis_pelaksanaan_rencana", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "jenis_kegiatan_rencana", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "tgl_mulai_rencana", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "tgl_selesai_rencana", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "tgl_mulai_laporan", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "tgl_selesai_laporan", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "provinsi_rencana", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "provinsi_laporan", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "kokab_rencana", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "kokab_laporan", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "anggaran_awp", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "anggaran_pok", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "anggaran_laporan", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "jml_event_rencana", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "jml_narsum_rencana", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "jml_peserta_rencana", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "jml_peserta_laporan", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "jml_peserta_pria_laporan", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "jml_peserta_wanita_laporan", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "penanggung_jawab", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "pic_lsp", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "risiko", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "impak", "INT64", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "potensi", "INT64", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "nilai_qae", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "compliance_monev", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "ekonomis_monev", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "efektif_monev", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "efisien_monev", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "compliance_peserta", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "ekonomis_peserta", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "efektif_peserta", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "efisien_peserta", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "status_rencana", "INT64", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "status_laporan", "INT64", "NULLABLE"
                )
            )

            repoVTableKegiatan.getAllKegiatan().forEach { d ->
                val data = mutableMapOf<String, String>()

                d.komponen?.let {
                    data["komponen"] = it
                }
                d.kode_kegiatan?.let {
                    data["kode_kegiatan"] = it
                }
                d.kegiatan?.let {
                    data["kegiatan"] = it
                }
                d.tahun?.let {
                    data["tahun"] = it.toString()
                }
                d.bulan?.let {
                    data["bulan"] = it.toString()
                }
                d.jenis_pelaksanaan_rencana?.let {
                    data["jenis_pelaksanaan_rencana"] = it
                }
                d.jenis_kegiatan_rencana?.let {
                    data["jenis_kegiatan_rencana"] = it
                }
                d.tgl_mulai_rencana?.let {
                    data["tgl_mulai_rencana"] = dateToString(date = it, format = "yyyy-MM-dd")
                }
                d.tgl_selesai_rencana?.let {
                    data["tgl_selesai_rencana"] = dateToString(date = it, format = "yyyy-MM-dd")
                }
                d.tgl_mulai_laporan?.let {
                    data["tgl_mulai_laporan"] = dateToString(date = it, format = "yyyy-MM-dd")
                }
                d.tgl_selesai_laporan?.let {
                    data["tgl_selesai_laporan"] = dateToString(date = it, format = "yyyy-MM-dd")
                }
                d.provinsi_rencana?.let {
                    data["provinsi_rencana"] = it
                }
                d.provinsi_laporan?.let {
                    data["provinsi_laporan"] = it
                }
                d.kokab_rencana?.let {
                    data["kokab_rencana"] = it
                }
                d.kokab_laporan?.let {
                    data["kokab_laporan"] = it
                }
                d.anggaran_awp?.let {
                    data["anggaran_awp"] = it.toString()
                }
                d.anggaran_pok?.let {
                    data["anggaran_pok"] = it.toString()
                }
                d.anggaran_laporan?.let {
                    data["anggaran_laporan"] = it.toString()
                }
                d.jml_event_rencana?.let {
                    data["jml_event_rencana"] = it.toString()
                }
                d.jml_narsum_rencana?.let {
                    data["jml_narsum_rencana"] = it.toString()
                }
                d.jml_peserta_rencana?.let {
                    data["jml_peserta_rencana"] = it.toString()
                }
                d.jml_peserta_laporan?.let {
                    data["jml_peserta_laporan"] = it.toString()
                }
                d.jml_peserta_pria_laporan?.let {
                    data["jml_peserta_pria_laporan"] = it.toString()
                }
                d.jml_peserta_wanita_laporan?.let {
                    data["jml_peserta_wanita_laporan"] = it.toString()
                }
                d.penanggung_jawab?.let {
                    data["penanggung_jawab"] = it
                }
                d.pic_lsp?.let {
                    data["pic_lsp"] = it
                }
                d.risiko?.let {
                    data["risiko"] = it
                }
                d.impak?.let {
                    data["impak"] = it.toString()
                }
                d.potensi?.let {
                    data["potensi"] = it.toString()
                }
                d.nilai_qae?.let {
                    data["nilai_qae"] = it.toString()
                }
                d.compliance_monev?.let {
                    data["compliance_monev"] = it.toString()
                }
                d.ekonomis_monev?.let {
                    data["ekonomis_monev"] = it.toString()
                }
                d.efektif_monev?.let {
                    data["efektif_monev"] = it.toString()
                }
                d.efisien_monev?.let {
                    data["efisien_monev"] = it.toString()
                }
                d.compliance_peserta?.let {
                    data["compliance_peserta"] = it.toString()
                }
                d.ekonomis_peserta?.let {
                    data["ekonomis_peserta"] = it.toString()
                }
                d.efektif_peserta?.let {
                    data["efektif_peserta"] = it.toString()
                }
                d.efisien_peserta?.let {
                    data["efisien_peserta"] = it.toString()
                }
                d.status_rencana?.let {
                    data["status_rencana"] = it.toString()
                }
                d.status_laporan?.let {
                    data["status_laporan"] = it.toString()
                }

                dashboardMainTable.data?.add(data)
            }

            dashboardMain.tables = dashboardMainTable


            return ObjectMapper().writeValueAsString(dashboardMain)
        } catch (e: Exception) {
            throw e
        }
    }

    fun eventMessage(): String {
        try {
            val dashboardMain = DashboardMain()
            dashboardMain.datasetId = "pmrms"
            dashboardMain.datasetOwner = "pmrms"

            val dashboardMainTable = DashboardMainTable()
            dashboardMainTable.tableName = "event_prod"

            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "komponen", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "kode_kegiatan", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "kegiatan", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "event", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "tahun", "INT64", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "bulan", "INT64", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "jenis_pelaksanaan_rencana", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "jenis_kegiatan_rencana", "STRING", "NULLABLE"
                )
            )

            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "tgl_mulai_rencana", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "tgl_selesai_rencana", "STRING", "NULLABLE"
                )
            )

            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "tgl_mulai_laporan", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "tgl_selesai_laporan", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "provinsi_rencana", "STRING", "NULLABLE"
                )
            )

            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "provinsi_laporan", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "kokab_rencana", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "kokab_laporan", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "anggaran_rencana", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "anggaran_laporan", "NUMERIC", "NULLABLE"
                )
            )

            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "jml_peserta_rencana", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "jml_peserta_laporan", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "jml_peserta_wanita_laporan", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "jml_peserta_pria_laporan", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "penanggung_jawab", "STRING", "NULLABLE"
                )
            )

            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "status_rencana", "INT64", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "compliance_monev", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "ekonomis_monev", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "efektif_monev", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "efisien_monev", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "compliance_peserta", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "ekonomis_peserta", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "efektif_peserta", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "efisien_peserta", "NUMERIC", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "status_laporan", "INT64", "NULLABLE"
                )
            )

            repoVTableEvent.getAllEvent().forEach { d ->
                val data = mutableMapOf<String, String>()

                d.komponen?.let {
                    data["komponen"] = it
                }
                d.kode_kegiatan?.let {
                    data["kode_kegiatan"] = it
                }
                d.kegiatan?.let {
                    data["kegiatan"] = it
                }
                d.event?.let {
                    data["event"] = it
                }
                d.tahun?.let {
                    data["tahun"] = it.toString()
                }
                d.bulan?.let {
                    data["bulan"] = it.toString()
                }
                d.jenis_pelaksanaan_rencana?.let {
                    data["jenis_pelaksanaan_rencana"] = it
                }
                d.jenis_kegiatan_rencana?.let {
                    data["jenis_kegiatan_rencana"] = it
                }
                d.tgl_mulai_rencana?.let {
                    data["tgl_mulai_rencana"] = dateToString(date = it, format = "yyyy-MM-dd")
                }
                d.tgl_selesai_rencana?.let {
                    data["tgl_selesai_rencana"] = dateToString(date = it, format = "yyyy-MM-dd")
                }
                d.tgl_mulai_laporan?.let {
                    data["tgl_mulai_laporan"] = dateToString(date = it, format = "yyyy-MM-dd")
                }
                d.tgl_selesai_laporan?.let {
                    data["tgl_selesai_laporan"] = dateToString(date = it, format = "yyyy-MM-dd")
                }
                d.provinsi_rencana?.let {
                    data["provinsi_rencana"] = it
                }
                d.provinsi_laporan?.let {
                    data["provinsi_laporan"] = it
                }
                d.kokab_rencana?.let {
                    data["kokab_rencana"] = it
                }
                d.kokab_laporan?.let {
                    data["kokab_laporan"] = it
                }
                d.anggaran_rencana?.let {
                    data["anggaran_rencana"] = it.toString()
                }
                d.anggaran_laporan?.let {
                    data["anggaran_laporan"] = it.toString()
                }
                d.anggaran_laporan?.let {
                    data["anggaran_laporan"] = it.toString()
                }
                d.jml_peserta_rencana?.let {
                    data["jml_peserta_rencana"] = it.toString()
                }
                d.jml_peserta_laporan?.let {
                    data["jml_peserta_laporan"] = it.toString()
                }
                d.jml_peserta_pria_laporan?.let {
                    data["jml_peserta_pria_laporan"] = it.toString()
                }
                d.jml_peserta_wanita_laporan?.let {
                    data["jml_peserta_wanita_laporan"] = it.toString()
                }
                d.penanggung_jawab?.let {
                    data["penanggung_jawab"] = it
                }
                d.status_rencana?.let {
                    data["status_rencana"] = it.toString()
                }
                d.compliance_monev?.let {
                    data["compliance_monev"] = it.toString()
                }
                d.ekonomis_monev?.let {
                    data["ekonomis_monev"] = it.toString()
                }
                d.efektif_monev?.let {
                    data["efektif_monev"] = it.toString()
                }
                d.efisien_monev?.let {
                    data["efisien_monev"] = it.toString()
                }
                d.compliance_peserta?.let {
                    data["compliance_peserta"] = it.toString()
                }
                d.ekonomis_peserta?.let {
                    data["ekonomis_peserta"] = it.toString()
                }
                d.efektif_peserta?.let {
                    data["efektif_peserta"] = it.toString()
                }
                d.efisien_peserta?.let {
                    data["efisien_peserta"] = it.toString()
                }
                d.status_laporan?.let {
                    data["status_laporan"] = it.toString()
                }

                dashboardMainTable.data?.add(data)
            }

            dashboardMain.tables = dashboardMainTable

            return ObjectMapper().writeValueAsString(dashboardMain)
        } catch (e: Exception) {
            throw e
        }
    }

    fun informasiResikoMessage(): String {
        try {
            val dashboardMain = DashboardMain()
            dashboardMain.datasetId = "pmrms"
            dashboardMain.datasetOwner = "pmrms"

            val dashboardMainTable = DashboardMainTable()
            dashboardMainTable.tableName = "informasi_resiko_prod"

            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "btrim", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "impact_risk", "INT64", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "potential_risk", "INT64", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "code", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "name", "STRING", "NULLABLE"
                )
            )

            repoVTableInformasiResiko.getAllInformasiResiko().forEach {
                val data = mutableMapOf<String, String>()
                data["btrim"] = it.btrim ?: ""
                data["impact_risk"] = it.impact_risk?.toString() ?: ""
                data["potential_risk"] = it.potential_risk?.toString() ?: ""
                data["code"] = it.code ?: ""
                data["name"] = it.name ?: ""
                dashboardMainTable.data?.add(data)

            }

            dashboardMain.tables = dashboardMainTable

            return ObjectMapper().writeValueAsString(dashboardMain)
        } catch (e: Exception) {
            throw e
        }
    }

    fun InformasiBudgetRealisasiMessage(): String {
        try {
            val dashboardMain = DashboardMain()
            dashboardMain.datasetId = "pmrms"
            dashboardMain.datasetOwner = "pmrms"

            val dashboardMainTable = DashboardMainTable()
            dashboardMainTable.tableName = "informasi_budget_realisasi_prod"

            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "code", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "name", "STRING", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "budget_year", "INT64", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "budget_ceiling", "INT64", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "rra_budget", "INT64", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "participant_male", "INT64", "NULLABLE"
                )
            )
            dashboardMainTable.columns?.add(
                DashboardMainTableCulumn(
                    "participant_female", "INT64", "NULLABLE"
                )
            )

            repoVTableInformasiBudgetRealisasi.getAllInformasiBudgetRealisasi().forEach { d ->
                val data = mutableMapOf<String, String>()

                d.code?.let {
                    data["code"] = it
                }
                d.name?.let {
                    data["name"] = it
                }
                d.budget_year?.let {
                    data["budget_year"] = it.toString()
                }
                d.budget_ceiling?.let {
                    data["budget_ceiling"] = it.toString()
                }
                d.rra_budget?.let {
                    data["rra_budget"] = it.toString()
                }
                d.participant_male?.let {
                    data["participant_male"] = it.toString()
                }
                d.participant_male?.let {
                    data["participant_female"] = it.toString()
                }
                dashboardMainTable.data?.add(data)
            }

            dashboardMain.tables = dashboardMainTable

            return ObjectMapper().writeValueAsString(dashboardMain)
        } catch (e: Exception) {
            throw e
        }
    }
}
