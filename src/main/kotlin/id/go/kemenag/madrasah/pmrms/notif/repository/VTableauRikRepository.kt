package id.go.kemenag.madrasah.pmrms.notif.repository

import id.go.kemenag.madrasah.pmrms.notif.pojo.VTableauRik
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface VTableauRikRepository : JpaRepository<VTableauRik, String> {

    @Query("select v from VTableauRik v")
    fun getAllRik(): List<VTableauRik>
}
