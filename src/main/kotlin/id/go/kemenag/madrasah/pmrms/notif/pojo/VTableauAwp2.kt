package id.go.kemenag.madrasah.pmrms.notif.pojo

import com.fasterxml.jackson.annotation.JsonFormat
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "v_tableau_awp2", schema = "public")
data class VTableauAwp2(
    @Id
    @Column(name = "id")
    var id: Int? = null,

    @Column(name = "komponen")
    var komponen: String? = null,

    @Column(name = "kode_kegiatan")
    var kode_kegiatan: String? = null,

    @Column(name = "kegiatan")
    var kegiatan: String? = null,

    @Column(name = "tahun")
    var tahun: Int? = null,

    @Column(name = "jenis_pelaksanaan")
    var jenis_pelaksanaan: String? = null,

    @Column(name = "jenis_kegiatan")
    var jenis_kegiatan: String? = null,

    @Column(name = "tgl_mulai")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var tgl_mulai: Date? = null,

    @Column(name = "tgl_selesai")
    @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+7")
    var tgl_selesai: Date? = null,

    @Column(name = "provinsi")
    var provinsi: String? = null,

    @Column(name = "kokab")
    var kokab: String? = null,

    @Column(name = "anggaran_awp")
    var anggaran_awp: Long? = null,

    @Column(name = "jml_event")
    var jml_event: Long? = null,

    @Column(name = "jml_narsum")
    var jml_narsum: Long? = null,

    @Column(name = "jml_peserta")
    var jml_peserta: Long? = null,

)
