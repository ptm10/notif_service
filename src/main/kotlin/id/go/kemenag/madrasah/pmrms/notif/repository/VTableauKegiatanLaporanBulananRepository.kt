package id.go.kemenag.madrasah.pmrms.notif.repository


import id.go.kemenag.madrasah.pmrms.notif.pojo.VTableauKegiatanLaporanBulanan
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface VTableauKegiatanLaporanBulananRepository : JpaRepository<VTableauKegiatanLaporanBulanan, String> {

    @Query("select v from VTableauKegiatanLaporanBulanan v")
    fun getAllKegiatanLaporanBulanan(): List<VTableauKegiatanLaporanBulanan>
}
