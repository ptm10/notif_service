package id.go.kemenag.madrasah.pmrms.notif.controller

import id.go.kemenag.madrasah.pmrms.notif.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.notif.service.TableauInformasiResikoService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Api(tags = ["Tableau Informasi Resiko"], description = "Tableau Informasi Resiko API")
@RestController
@RequestMapping(path = ["tableau-informasi-resiko"])
class TableauInformasiResikoController {

    @Autowired
    private lateinit var service: TableauInformasiResikoService

    @GetMapping(value = ["get-all-informasi-resiko"], produces = ["application/json"])
    fun getAllInformasiResiko(): ResponseEntity<ReturnData> {
        return service.getAllInformasiResiko()
    }
}
