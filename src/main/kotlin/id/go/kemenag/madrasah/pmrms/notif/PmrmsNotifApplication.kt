package id.go.kemenag.madrasah.pmrms.notif

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import java.util.*
import javax.annotation.PostConstruct

@SpringBootApplication
open class PmrmsNotifApplication {

    @PostConstruct
    fun started() {
        // set JVM timezone as UTC
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+7"))
    }
}

fun main(args: Array<String>) {
    runApplication<PmrmsNotifApplication>(*args)
}
