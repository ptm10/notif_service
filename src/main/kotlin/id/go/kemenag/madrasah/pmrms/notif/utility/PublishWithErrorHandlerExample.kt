package id.go.kemenag.madrasah.pmrms.notif.utility

import com.google.api.core.ApiFuture
import com.google.api.core.ApiFutureCallback
import com.google.api.core.ApiFutures
import com.google.api.gax.rpc.ApiException
import com.google.cloud.pubsub.v1.Publisher
import com.google.common.util.concurrent.MoreExecutors
import com.google.protobuf.ByteString
import com.google.pubsub.v1.PubsubMessage
import com.google.pubsub.v1.TopicName
import java.io.IOException
import java.util.concurrent.TimeUnit


class PublishWithErrorHandlerExample {

    @Throws(IOException::class, InterruptedException::class)
    fun publishWithErrorHandlerExample(projectId: String?, topicId: String?) {
        val topicName = TopicName.of(projectId, topicId)
        var publisher: Publisher? = null
        try {
            // Create a publisher instance with default settings bound to the topic
            publisher = Publisher.newBuilder(topicName).build()
            val messages: List<String> = listOf("first message", "second message")
            for (message in messages) {
                val data: ByteString = ByteString.copyFromUtf8(message)
                val pubsubMessage = PubsubMessage.newBuilder().setData(data).build()

                // Once published, returns a server-assigned message id (unique within the topic)
                val future: ApiFuture<String> = publisher.publish(pubsubMessage)

                // Add an asynchronous callback to handle success / failure
                ApiFutures.addCallback(
                    future,
                    object : ApiFutureCallback<String> {
                        override fun onFailure(throwable: Throwable) {
                            if (throwable is ApiException) {
                                // details on the API exception
                                println(throwable.statusCode.code)
                                println(throwable.isRetryable)
                            }
                            println("Error publishing message : $message")
                        }

                        override fun onSuccess(messageId: String) {
                            // Once published, returns server-assigned message ids (unique within the topic)
                            println("Published message ID: $messageId")
                        }
                    },
                    MoreExecutors.directExecutor()
                )
            }
        }
        finally {
//            if (publisher != null) {
//                // When finished with the publisher, shutdown to free up resources.
//                publisher.shutdown()
//                publisher.awaitTermination(1, TimeUnit.MINUTES)
//            }
        }
    }
}
