package id.go.kemenag.madrasah.pmrms.notif.repository

import id.go.kemenag.madrasah.pmrms.notif.pojo.VTableauInformasiBudgetRealisasi
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface VTableauInformasiBudgetRealisasiRepository : JpaRepository<VTableauInformasiBudgetRealisasi, String> {

    @Query("select v from VTableauInformasiBudgetRealisasi v")
    fun getAllInformasiBudgetRealisasi(): List<VTableauInformasiBudgetRealisasi>
}
