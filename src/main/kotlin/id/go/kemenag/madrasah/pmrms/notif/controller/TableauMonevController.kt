package id.go.kemenag.madrasah.pmrms.notif.controller

import id.go.kemenag.madrasah.pmrms.notif.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.notif.service.TableauMonevService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Api(tags = ["Tableau Monev"], description = "Tableau Monev API")
@RestController
@RequestMapping(path = ["tableau-monev"])
class TableauMonevController {

    @Autowired
    private lateinit var service: TableauMonevService

    @GetMapping(value = ["get-all-monev"], produces = ["application/json"])
    fun getAllMonev(): ResponseEntity<ReturnData> {
        return service.getAllMonev()
    }
}
