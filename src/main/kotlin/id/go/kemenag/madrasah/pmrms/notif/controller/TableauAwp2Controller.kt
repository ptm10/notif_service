package id.go.kemenag.madrasah.pmrms.notif.controller

import id.go.kemenag.madrasah.pmrms.notif.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.notif.service.TableauAwp2Service
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Api(tags = ["Tableau Awp"], description = "Tableau Awp API")
@RestController
@RequestMapping(path = ["tableau-awp2"])
class TableauAwp2Controller {

    @Autowired
    private lateinit var service: TableauAwp2Service

    @GetMapping(value = ["get-all-awp2"], produces = ["application/json"])
    fun getAllAwp(): ResponseEntity<ReturnData> {
        return service.getAllAwp()
    }
}
