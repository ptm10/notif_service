package id.go.kemenag.madrasah.pmrms.notif.service

import id.go.kemenag.madrasah.pmrms.notif.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.notif.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.notif.repository.VTableauKegiatanRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Service
class TableauKegiatanService {

    @Autowired
    private lateinit var repoVTableauKegiatan: VTableauKegiatanRepository

    fun getAllKegiatan(): ResponseEntity<ReturnData> {
        try {
            return responseSuccess(data = repoVTableauKegiatan.getAllKegiatan())
        } catch (e: Exception) {
            throw e
        }
    }
}
