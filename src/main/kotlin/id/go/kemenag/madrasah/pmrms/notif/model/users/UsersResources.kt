package id.go.kemenag.madrasah.pmrms.notif.model.users

import id.go.kemenag.madrasah.pmrms.notif.model.users.Position
import java.util.*

data class UsersResources(

    var id: String? = UUID.randomUUID().toString(),

    var positionId: String? = null,

    var position: Position? = null,

    var phoneNumber: String? = null,

    var supervisiorId: String? = null,
)
