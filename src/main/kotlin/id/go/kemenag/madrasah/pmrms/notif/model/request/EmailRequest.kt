package id.go.kemenag.madrasah.pmrms.notif.model.request

import id.go.kemenag.madrasah.pmrms.notif.constant.*
import javax.validation.constraints.NotEmpty

data class EmailRequest(

    @field:NotEmpty(message = "Penerima $VALIDATOR_MSG_REQUIRED")
    var to: String? = null,

    @field:NotEmpty(message = "Subjek $VALIDATOR_MSG_REQUIRED")
    var subject: String? = null,

    @field:NotEmpty(message = "Pesan $VALIDATOR_MSG_REQUIRED")
    var message: String? = null
)

