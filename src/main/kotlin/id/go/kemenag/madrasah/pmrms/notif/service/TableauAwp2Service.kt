package id.go.kemenag.madrasah.pmrms.notif.service

import id.go.kemenag.madrasah.pmrms.notif.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.notif.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.notif.repository.VTableauAwp2Repository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Service
class TableauAwp2Service {

    @Autowired
    private lateinit var repoVTableauAwp2: VTableauAwp2Repository

    fun getAllAwp(): ResponseEntity<ReturnData> {
        try {
            return responseSuccess(data = repoVTableauAwp2.getAllAwp())
        } catch (e: Exception) {
            throw e
        }
    }
}
