package id.go.kemenag.madrasah.pmrms.notif.helpers

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.google.api.core.ApiFuture
import com.google.api.core.ApiFutureCallback
import com.google.api.core.ApiFutures
import com.google.api.gax.rpc.ApiException
import com.google.cloud.spring.pubsub.core.publisher.PubSubPublisherTemplate
import com.google.common.util.concurrent.MoreExecutors
import com.google.protobuf.ByteString
import com.google.pubsub.v1.PubsubMessage
import id.go.kemenag.madrasah.pmrms.notif.model.users.Users
import org.springframework.core.env.Environment
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.context.support.WebApplicationContextUtils
import java.io.IOException
import java.net.MalformedURLException
import java.net.URL
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.servlet.http.HttpServletRequest


fun getUserLogin(): Users? {
    return try {
        val principal = SecurityContextHolder.getContext().authentication.principal as Any
        val objectMapper = ObjectMapper()
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        objectMapper.readValue(principal.toString(), Users::class.java)
    } catch (e: Exception) {
        null
    }
}

@Throws(MalformedURLException::class)
fun getBaseUrl(request: HttpServletRequest): String {
    try {
        val requestURL = URL(request.requestURL.toString())
        val port = if (requestURL.port == -1) "" else ":" + requestURL.port
        return requestURL.protocol + "://" + requestURL.host + port + request.contextPath + "/"
    } catch (e: Exception) {
        throw e
    }
}

fun getFullUrl(request: HttpServletRequest): String {
    val requestURL = StringBuilder(request.requestURL.toString())
    val queryString = request.queryString
    return if (queryString == null) {
        requestURL.toString()
    } else {
        requestURL.append('?').append(queryString).toString()
    }
}

fun getEnvFromHttpServletRequest(httpServletRequest: HttpServletRequest, key: String): String {
    var rData = ""
    try {
        val servletContext = httpServletRequest.session.servletContext
        val webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext)
        val env: Environment = webApplicationContext!!.getBean(Environment::class.java)
        rData = env.getProperty(key, "")
        println(rData)
    } catch (_: Exception) {

    }
    return rData
}

fun dateToString(date: Date?, format: String? = "dd-MM-yyyy"): String {
    var rDate = ""
    try {
        val dform: DateFormat = SimpleDateFormat(format)
        rDate = dform.format(date)
    } catch (_: Exception) {

    }
    return rDate
}

fun publishMessages(pubSubTemplate: PubSubPublisherTemplate, topicId: String?, messages: List<String>) {
    try {
        messages.forEach {
            pubSubTemplate.publish(topicId, it).addCallback({ s ->
                println("publishMessagesSuccess : $s")
            }, { e ->
                e.printStackTrace()
            })
        }
    } catch (e: Exception) {
        throw e
    }
}

@Throws(IOException::class, InterruptedException::class)
fun publishWithErrorHandler(pubSubTemplate: PubSubPublisherTemplate, topicId: String?, messages: List<String>) {
    val publisher = pubSubTemplate.publisherFactory.createPublisher(topicId)
    try {
        messages.forEach {
            try {
                // Create a publisher instance with default settings bound to the topic
                val data: ByteString = ByteString.copyFromUtf8(it)
                val pubsubMessage = PubsubMessage.newBuilder().setData(data).build()

                // Once published, returns a server-assigned message id (unique within the topic)
                val future: ApiFuture<String> = publisher.publish(pubsubMessage)

                // Add an asynchronous callback to handle success / failure
                ApiFutures.addCallback(
                    future, object : ApiFutureCallback<String> {
                        override fun onFailure(throwable: Throwable) {
                            if (throwable is ApiException) {
                                // details on the API exception
                                println(throwable.statusCode.code)
                                println(throwable.isRetryable)
                            }
                        }

                        override fun onSuccess(messageId: String) {
                            // Once published, returns server-assigned message ids (unique within the topic)
                            println("Published message ID: $messageId")
                        }
                    }, MoreExecutors.directExecutor()
                )
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    } finally {
        if (publisher != null) {
            // When finished with the publisher, shutdown to free up resources.
            publisher.shutdown()
            publisher.awaitTermination(1, TimeUnit.MINUTES)
        }
    }
}
