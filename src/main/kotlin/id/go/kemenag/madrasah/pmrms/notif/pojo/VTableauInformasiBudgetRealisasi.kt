package id.go.kemenag.madrasah.pmrms.notif.pojo

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "v_tableau_informasi_budget_realisasi", schema = "public")
data class VTableauInformasiBudgetRealisasi(
    @Id
    @Column(name = "code")
    var code: String? = null,

    @Column(name = "name")
    var name: String? = null,

    @Column(name = "budget_year")
    var budget_year: Int? = null,

    @Column(name = "budget_ceiling")
    var budget_ceiling: Int? = null,

    @Column(name = "rra_budget")
    var rra_budget: Int? = null,

    @Column(name = "participant_male")
    var participant_male: Int? = null,

    @Column(name = "participant_female")
    var participant_female: Int? = null,

)
