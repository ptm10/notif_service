package id.go.kemenag.madrasah.pmrms.notif.service

import id.go.kemenag.madrasah.pmrms.notif.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.notif.model.request.EmailRequest
import id.go.kemenag.madrasah.pmrms.notif.model.response.ReturnData
import org.apache.velocity.Template
import org.apache.velocity.VelocityContext
import org.apache.velocity.app.VelocityEngine
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.stereotype.Service
import java.io.StringWriter
import javax.mail.internet.MimeMessage


@Service
class EmailService {

    @Value("\${app.name}")
    private lateinit var appName: String

    @Autowired
    private lateinit var javaMailSender: JavaMailSender

    @Value("\${spring.mail.from}")
    private lateinit var emailFrom: String

    @Value("\${app.website.url}")
    private lateinit var websiteUrl: String

    @Value("\${app.logo.url}")
    private lateinit var logoUrl: String

    @Autowired
    private lateinit var velocityEngine: VelocityEngine

    fun sendEamil(request: EmailRequest): ResponseEntity<ReturnData> {
        return try {
            Thread {
                sendEmail(request.to, request.subject, request.message)
            }.start()
            responseSuccess()
        } catch (e: Exception) {
            throw e
        }
    }

    private fun sendEmail(
        to: String?,
        subject: String?,
        messageEmail: String?,
    ) {
        try {
            val template: Template = velocityEngine.getTemplate("velocity/email-template.vm", "UTF-8")
            val velocityContext = VelocityContext()
            velocityContext.put("subject", subject ?: "")
            velocityContext.put("message", messageEmail ?: "")
            velocityContext.put("logoUrl", websiteUrl + logoUrl)
            val stringWriter = StringWriter()
            template.merge(velocityContext, stringWriter)


            val mimeMessage: MimeMessage = javaMailSender.createMimeMessage()
            val mimeMessageHelper = MimeMessageHelper(mimeMessage, true)
            mimeMessageHelper.setTo(to ?: "")
            mimeMessageHelper.setText(stringWriter.toString(), true)
            mimeMessageHelper.setSubject("$subject  $appName")
            mimeMessageHelper.setFrom(emailFrom)

            javaMailSender.send(mimeMessage)
        } catch (e: Exception) {
            throw e
        }
    }
}
