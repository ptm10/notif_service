package id.go.kemenag.madrasah.pmrms.notif.service

import id.go.kemenag.madrasah.pmrms.notif.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.notif.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.notif.repository.VTableauAwpRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import java.time.Year

@Service
class TableauAwpService {

    @Autowired
    private lateinit var repoVTableauAwp: VTableauAwpRepository

    fun getByYear(year: Int?): ResponseEntity<ReturnData> {
        try {
            return responseSuccess(data = repoVTableauAwp.getByYear(year ?: Year.now().value))
        } catch (e: Exception) {
            throw e
        }
    }
}
