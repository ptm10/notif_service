package id.go.kemenag.madrasah.pmrms.notif.pojo

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "v_tableau_informasi_resiko", schema = "public")
data class VTableauInformasiResiko(
    @Id
    @Column(name = "code")
    var code: String? = null,

    @Column(name = "btrim")
    var btrim: String? = null,

    @Column(name = "impact_risk")
    var impact_risk: Int? = null,

    @Column(name = "potential_risk")
    var potential_risk: Int? = null,

    @Column(name = "name")
    var name: String? = null,

)