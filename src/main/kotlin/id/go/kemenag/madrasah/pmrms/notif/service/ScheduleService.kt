package id.go.kemenag.madrasah.pmrms.notif.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

@Service
class ScheduleService {

    @Autowired
    private lateinit var pubSubService: PubSubService

    @Scheduled(cron = "0 0 0 * * *")
    fun pubSubSchedule() {
        println("begin pubSubSchedule")
        pubSubService.sendAllMessage()
        println("end pubSubSchedule")
    }
}
