package id.go.kemenag.madrasah.pmrms.notif.pojo

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "v_tableau_monev", schema = "public")
data class VTableauMonev(

    @Id
    @Column(name = "id")
    var id: Int? = null,

    @Column(name = "tahun")
    var tahun: Int? = null,

    @Column(name = "rc_komponen")
    var rc_komponen: String? = null,

    @Column(name = "rc_sub_komponen")
    var rc_sub_komponen: String? = null,

    @Column(name = "rc_code")
    var rc_code: String? = null,

    @Column(name = "rc_nama")
    var rc_nama: String? = null,

    @Column(name = "rc_parent")
    var rc_parent: String? = null,

    @Column(name = "rc_status2020")
    var rc_status2020: Int? = null,

    @Column(name = "rc_status2021")
    var rc_status2021: Int? = null,

    @Column(name = "rc_status2022")
    var rc_status2022: Int? = null,

    @Column(name = "rc_status2023")
    var rc_status2023: Int? = null,

    @Column(name = "rc_status2024")
    var rc_status2024: Int? = null,

    @Column(name = "si_code")
    var si_code: String? = null,

    @Column(name = "si_name")
    var si_name: String? = null,

    @Column(name = "pdo_code")
    var pdo_code: String? = null,

    @Column(name = "pdo_name")
    var pdo_name: String? = null,

    @Column(name = "iri_code")
    var iri_code: String? = null,

    @Column(name = "iri_name")
    var iri_name: String? = null,

    @Column(name = "si_baseline")
    var si_baseline: String? = null,

    @Column(name = "si_target_total")
    var si_target_total: String? = null,

    @Column(name = "si_target2020")
    var si_target2020: String? = null,

    @Column(name = "si_target2021")
    var si_target2021: String? = null,

    @Column(name = "si_target2022")
    var si_target2022: String? = null,

    @Column(name = "si_target2023")
    var si_target2023: String? = null,

    @Column(name = "si_target2024")
    var si_target2024: String? = null,

    @Column(name = "kegiatan_kode")
    var kegiatan_kode: String? = null,

    @Column(name = "kegiatan_name")
    var kegiatan_name: String? = null,
)
