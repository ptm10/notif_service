package id.go.kemenag.madrasah.pmrms.notif.controller

import id.go.kemenag.madrasah.pmrms.notif.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.notif.service.PubSubService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Api(tags = ["Pup/Sub"], description = "Pup/Sub API")
@RestController
@RequestMapping(path = ["pup-sub"])
class PupSubController {

    @Autowired
    private lateinit var service: PubSubService

    @PostMapping(value = ["send-all-message"], produces = ["application/json"])
    fun sendAllMessage(){
        service.sendAllMessage()
    }

    @PostMapping(value = ["test-send-message"], produces = ["application/json"])
    fun testSendMessage(): ResponseEntity<ReturnData> {
        return service.testSendMessage()
    }

    @PostMapping(value = ["send-message-awp"], produces = ["application/json"])
    fun sendMessageAwp(): ResponseEntity<ReturnData> {
        return service.sendMessageAwp()
    }

    @PostMapping(value = ["send-message-awp2"], produces = ["application/json"])
    fun sendMessageAwp2(): ResponseEntity<ReturnData> {
        return service.sendMessageAwp2()
    }

    @PostMapping(value = ["send-message-monev"], produces = ["application/json"])
    fun sendMessageMonev(): ResponseEntity<ReturnData> {
        return service.sendMessageMonev()
    }

    @PostMapping(value = ["send-message-rik"], produces = ["application/json"])
    fun sendMessageRik(): ResponseEntity<ReturnData> {
        return service.sendMessageRik()
    }

    @PostMapping(value = ["send-message-kegiatan-laporan-bulanan"], produces = ["application/json"])
    fun sendMessageKegiatanLaporanBulanan(): ResponseEntity<ReturnData> {
        return service.sendMessageKegiatanLaporanBulanan()
    }

    @PostMapping(value = ["send-message-kegiatan"], produces = ["application/json"])
    fun sendMessageKegiatan(): ResponseEntity<ReturnData> {
        return service.sendMessageKegiatan()
    }
    @PostMapping(value = ["send-message-event"], produces = ["application/json"])
    fun sendMessageEvent(): ResponseEntity<ReturnData> {
        return service.sendMessageEvent()
    }
    @PostMapping(value = ["send-message-informasi-resiko"], produces = ["application/json"])
    fun sendMessageInformasiResiko(): ResponseEntity<ReturnData> {
        return service.sendMessageInformasiResiko()
    }
    @PostMapping(value = ["send-message-informasi-budget-realisasi"], produces = ["application/json"])
    fun sendMessageInformasiBudgetRealisasi(): ResponseEntity<ReturnData> {
        return service.sendMessageInformasiBudgetRealisasi()
    }

}
