package id.go.kemenag.madrasah.pmrms.notif.model.response.jwt

data class JwtAuthenticationResponse(
    val accessToken: String? = null,
    val tokenType: String? = null,
    val expired: Long = 0,
    val user: Any? = null
)
