package id.go.kemenag.madrasah.pmrms.notif.repository

import id.go.kemenag.madrasah.pmrms.notif.pojo.VTableauAwp2
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface VTableauAwp2Repository : JpaRepository<VTableauAwp2, String> {

    @Query("select v from VTableauAwp2 v")
    fun getAllAwp(): List<VTableauAwp2>
}
