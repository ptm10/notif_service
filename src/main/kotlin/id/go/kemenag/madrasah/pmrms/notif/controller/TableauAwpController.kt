package id.go.kemenag.madrasah.pmrms.notif.controller

import id.go.kemenag.madrasah.pmrms.notif.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.notif.service.TableauAwpService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@Api(tags = ["Tableau Awp"], description = "Tableau Awp API")
@RestController
@RequestMapping(path = ["tableau-awp"])
class TableauAwpController {

    @Autowired
    private lateinit var service: TableauAwpService

    @GetMapping(value = ["get-by-year"], produces = ["application/json"])
    fun getByYear(@RequestParam("year") year: Int?): ResponseEntity<ReturnData> {
        return service.getByYear(year)
    }
}
