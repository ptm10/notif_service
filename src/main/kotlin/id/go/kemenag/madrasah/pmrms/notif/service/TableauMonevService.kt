package id.go.kemenag.madrasah.pmrms.notif.service

import id.go.kemenag.madrasah.pmrms.notif.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.notif.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.notif.repository.VTableauMonevRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Service
class TableauMonevService {

    @Autowired
    private lateinit var repoVTableauMonev: VTableauMonevRepository

    fun getAllMonev(): ResponseEntity<ReturnData> {
        try {
            return responseSuccess(data = repoVTableauMonev.getAllMonev())
        } catch (e: Exception) {
            throw e
        }
    }
}
