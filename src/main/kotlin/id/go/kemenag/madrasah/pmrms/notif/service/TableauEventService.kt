package id.go.kemenag.madrasah.pmrms.notif.service

import id.go.kemenag.madrasah.pmrms.notif.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.notif.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.notif.repository.VTableauEventRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Service
class TableauEventService {

    @Autowired
    private lateinit var repoVTableauEvent: VTableauEventRepository

    fun getAllEvent(): ResponseEntity<ReturnData> {
        try {
            return responseSuccess(data = repoVTableauEvent.getAllEvent())
        } catch (e: Exception) {
            throw e
        }
    }
}
