package id.go.kemenag.madrasah.pmrms.notif.repository

import id.go.kemenag.madrasah.pmrms.notif.pojo.VTableauAwp
import org.springframework.data.jpa.repository.JpaRepository

interface VTableauAwpRepository : JpaRepository<VTableauAwp, String> {

    fun getByYear(year: Int?): List<VTableauAwp>
}
