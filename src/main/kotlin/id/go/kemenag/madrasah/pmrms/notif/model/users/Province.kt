package id.go.kemenag.madrasah.pmrms.notif.model.users

data class Province(

    var id: String? = null,

    var name: String? = null,

    var code: String? = null
)
