package id.go.kemenag.madrasah.pmrms.notif.constant

import java.io.File

const val ROOT_DIR = "D:\\app-files\\pmrms\\files\\"

val UPLOAD_DIR = "${ROOT_DIR}uploads${File.separator}"
val UPLOAD_USER_DIR = "${UPLOAD_DIR}users"
