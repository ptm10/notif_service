package id.go.kemenag.madrasah.pmrms.notif.controller

import id.go.kemenag.madrasah.pmrms.notif.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.notif.service.TableauKegiatanService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Api(tags = ["Tableau Kegiatan"], description = "Tableau Kegiatan API")
@RestController
@RequestMapping(path = ["tableau-kegiatan"])
class TableauKegiatanController {

    @Autowired
    private lateinit var service: TableauKegiatanService

    @GetMapping(value = ["get-all-kegiatan"], produces = ["application/json"])
    fun getAllKegiatan(): ResponseEntity<ReturnData> {
        return service.getAllKegiatan()
    }
}
