package id.go.kemenag.madrasah.pmrms.notif.service

import id.go.kemenag.madrasah.pmrms.notif.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.notif.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.notif.repository.VTableauInformasiResikoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Service
class TableauInformasiResikoService {

    @Autowired
    private lateinit var repoVTableauInformasiResiko: VTableauInformasiResikoRepository

    fun getAllInformasiResiko(): ResponseEntity<ReturnData> {
        try {
            return responseSuccess(data = repoVTableauInformasiResiko.getAllInformasiResiko())
        } catch (e: Exception) {
            throw e
        }
    }
}
