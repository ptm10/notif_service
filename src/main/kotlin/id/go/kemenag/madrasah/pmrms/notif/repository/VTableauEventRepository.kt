package id.go.kemenag.madrasah.pmrms.notif.repository

import id.go.kemenag.madrasah.pmrms.notif.pojo.VTableauEvent
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface VTableauEventRepository : JpaRepository<VTableauEvent, String> {

    @Query("select v from VTableauEvent v")
    fun getAllEvent(): List<VTableauEvent>
}
