package id.go.kemenag.madrasah.pmrms.notif.controller

import id.go.kemenag.madrasah.pmrms.notif.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.notif.service.TableauInformasiBudgetRealisasiService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Api(tags = ["Tableau Informasi Budget Realisasi Kegiatan"], description = "Tableau Informasi Budget Realisasi Kegiatan API")
@RestController
@RequestMapping(path = ["tableau-informasi-budget-realisasi"])
class TableauInformasiBudgetRealisasiController {

    @Autowired
    private lateinit var service: TableauInformasiBudgetRealisasiService

    @GetMapping(value = ["get-all-informasi-budget-realisasi"], produces = ["application/json"])
    fun getAllInformasiBudgetRealisasi(): ResponseEntity<ReturnData> {
        return service.getAllInformasiBudgetResiko()
    }
}
