package id.go.kemenag.madrasah.pmrms.notif.controller

import id.go.kemenag.madrasah.pmrms.notif.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.notif.service.TableauRikService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Api(tags = ["Tableau RIK"], description = "Tableau RIK API")
@RestController
@RequestMapping(path = ["tableau-rik"])
class TableauRikController {

    @Autowired
    private lateinit var service: TableauRikService

    @GetMapping(value = ["get-all-rik"], produces = ["application/json"])
    fun getAllRik(): ResponseEntity<ReturnData> {
        return service.getAllRik()
    }
}
