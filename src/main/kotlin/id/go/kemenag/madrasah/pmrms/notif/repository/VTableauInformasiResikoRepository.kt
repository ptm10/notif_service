package id.go.kemenag.madrasah.pmrms.notif.repository

import id.go.kemenag.madrasah.pmrms.notif.pojo.VTableauInformasiResiko
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface VTableauInformasiResikoRepository : JpaRepository<VTableauInformasiResiko, String> {

    @Query("select v from VTableauInformasiResiko v")
    fun getAllInformasiResiko(): List<VTableauInformasiResiko>
}
