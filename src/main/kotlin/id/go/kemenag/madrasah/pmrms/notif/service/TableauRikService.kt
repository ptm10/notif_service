package id.go.kemenag.madrasah.pmrms.notif.service

import id.go.kemenag.madrasah.pmrms.notif.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.notif.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.notif.repository.VTableauRikRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Service
class TableauRikService {

    @Autowired
    private lateinit var repoVTableauRik: VTableauRikRepository

    fun getAllRik(): ResponseEntity<ReturnData> {
        try {
            return responseSuccess(data = repoVTableauRik.getAllRik())
        } catch (e: Exception) {
            throw e
        }
    }
}
