package id.go.kemenag.madrasah.pmrms.notif.service

import id.go.kemenag.madrasah.pmrms.notif.helpers.responseSuccess
import id.go.kemenag.madrasah.pmrms.notif.model.response.ReturnData
import id.go.kemenag.madrasah.pmrms.notif.repository.VTableauKegiatanLaporanBulananRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Service
class TableauKegiatanLaporananBulananService {

    @Autowired
    private lateinit var repoVTableauKegiatangetLaporanBulanan: VTableauKegiatanLaporanBulananRepository

    fun getAllKegiatanLaporanBulanan(): ResponseEntity<ReturnData> {
        try {
            return responseSuccess(data = repoVTableauKegiatangetLaporanBulanan.getAllKegiatanLaporanBulanan())
        } catch (e: Exception) {
            throw e
        }
    }
}
